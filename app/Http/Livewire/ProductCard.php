<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProductCard extends Component
{
    public $product_id;
    public $title;
    public $imported_date;
    public $price;
    public $img_url;

    public function render()
    {
        return view('livewire.product-card');
    }
}
