<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ServiceModel extends Component
{
    public $service_id;
    public $title;
    public $img_url;
    public $description;
    public function render()
    {
        return view('livewire.service-model');
    }
}
