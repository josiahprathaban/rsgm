<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProductModel extends Component
{
    public $product_id;
    public $title;
    public $imported_date;
    public $price;
    public $img_url;
    public $stock;
    public $description;
    public function render()
    {
        return view('livewire.product-model');
    }
}
