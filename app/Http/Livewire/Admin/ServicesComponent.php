<?php

namespace App\Http\Livewire\Admin;

use App\Models\Service;
use Livewire\Component;
use Livewire\WithFileUploads;

class ServicesComponent extends Component
{
    public $name;
    public $description;
    public $price;
    public $image;
    public $ids;
    public $update_image;
    public $searchTerm;

    use WithFileUploads;

    public function render()
    {
        $searchTerm = '%' .$this->searchTerm .'%';
        $services = Service::where('name','LIKE',$searchTerm)->orderBy('id','DESC')->get();

        return view('livewire.admin.services-component',compact('services'))->layout('admin.layouts.app');
    }

    public function clear()
    {
        $this->price = '';
        $this->name = '';
        $this->description = '';
       
    }

    public function store()
    {
        $validate_data = $this->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,webp',
            'price' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        $image = $this->image->store('files','public');

        $services = new Service();
        $services->price = $this->price;
        $services->name = $this->name;
        $services->description = $this->description;
        $services->img = $image;
        $services->save();

        session()->flash('message', 'Service has been added successfully!');
        $this->clear();
        $this->emit('vegetableAdded');
    }

    public function edit($id)
    {
        $service = Service::where('id', $id)->first();
        $this->ids = $service->id;
        $this->price = $service->price;
        $this->name = $service->name;
        $this->description = $service->description;
        $this->update_image = $service->img;
        
    }

    public function update()
    {
        if ($this->ids) {
           
            $service = Service::find($this->ids);
            $service->price = $this->price;
            $service->name = $this->name;
            $service->description = $this->description;
            if($this->image){
                $image = $this->image->store('files','public');
                $service->img = $image;
            }
           
            $service->save();

            session()->flash('message', 'Service has been updated successfully!');
            $this->clear();
            $this->emit('vegetableUpdated');
        }
    }

}
