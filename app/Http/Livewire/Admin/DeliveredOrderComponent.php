<?php

namespace App\Http\Livewire\Admin;

use App\Models\User;
use Livewire\Component;

class DeliveredOrderComponent extends Component
{
    public $searchTerm;

    public function render()
    {
       

        $order_details = User::join('orders','orders.user_id','=','users.id')
        ->join('vegetables','vegetables.id','=','orders.vegetable_id')
        ->select('orders.*','users.name','vegetables.name as vegetable_name')
        ->where('orders.order_status','Delivered')
        ->where(function ($query) {
            $searchTerm = '%' .$this->searchTerm .'%';
            $query->where('users.name','LIKE',$searchTerm)
            ->orWhere('orders.id','LIKE',$searchTerm)
            ->orWhere('vegetables.name','LIKE',$searchTerm)
            ->orWhere('orders.quantity','LIKE',$searchTerm)
            ->orWhere('orders.total','LIKE',$searchTerm)
            ->orWhere('orders.created_at','LIKE',$searchTerm);

        })
        ->orderBy('orders.id','DESC')
        ->get();

        return view('livewire.admin.delivered-order-component',compact('order_details'))->layout('admin.layouts.app');
    }
}
