<?php

namespace App\Http\Livewire\Admin;

use App\Models\Vegetable;
use Livewire\Component;
use Livewire\WithFileUploads;

class VegetablesComponent extends Component
{
    public $name;
    public $description;
    public $date;
    public $price;
    public $stock;
    public $image;
    public $ids;
    public $add_stock;
    public $update_image;
    public $searchTerm;

    use WithFileUploads;
    public function render()
    {
        $searchTerm = '%' .$this->searchTerm .'%';
        $vegetables = Vegetable::where('name','LIKE',$searchTerm)->orderBy('id','DESC')->get();

        return view('livewire.admin.vegetables-component',compact('vegetables'))->layout('admin.layouts.app');
    }
    public function clear()
    {
        $this->price = '';
        $this->stock = '';
        $this->name = '';
        $this->description = '';
        $this->date = '';
       
    }
    public function store()
    {
        $validate_data = $this->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,webp',
            'price' => 'required',
            'stock' => 'required',
            'name' => 'required',
            'date' => 'required',
            'description' => 'required',
        ]);

        $image = $this->image->store('files','public');

        $vegetables = new Vegetable();
        $vegetables->price = $this->price;
        $vegetables->stock = $this->stock;
        $vegetables->name = $this->name;
        $vegetables->description = $this->description;
        $vegetables->import_date = $this->date;
        $vegetables->img = $image;
        $vegetables->save();

        session()->flash('message', 'Vegetable has been added successfully!');
        $this->clear();
        $this->emit('vegetableAdded');
    }

    public function edit($id)
    {
        $vegetable = Vegetable::where('id', $id)->first();
        $this->ids = $vegetable->id;
        $this->price = $vegetable->price;
        $this->stock = $vegetable->stock;
        $this->name = $vegetable->name;
        $this->description = $vegetable->description;
        $this->date = $vegetable->import_date;
        $this->update_image = $vegetable->img;
        $this-> date = date('Y-m-d', strtotime( $this->date)) ;
        
    }

    public function update()
    {
        if ($this->ids) {
           
            $vegetable = Vegetable::find($this->ids);
            $vegetable->price = $this->price;
            $vegetable->stock = $this->stock;
            $vegetable->name = $this->name;
            $vegetable->description = $this->description;
            $vegetable->import_date = $this->date;
            if($this->image){
                $image = $this->image->store('files','public');
                $vegetable->img = $image;
            }
           
            $vegetable->save();

            session()->flash('message', 'Vegetable has been updated successfully!');
            $this->clear();
            $this->emit('vegetableUpdated');
        }
    }

    public function addStock(){
        if ($this->ids) {
            $validate_data = $this->validate([
                'add_stock' => 'required',
            ]);

            $vegetable = Vegetable::find($this->ids);
            $vegetable->stock = $this->stock + $this-> add_stock;
            $vegetable->save();

            session()->flash('message', 'Vegetable stock has been added successfully!');
            $this->add_stock = '';
            $this->emit('stockUpdated');
        }
    }

    public function changeStatus($status, $id){
        $vegetable = Vegetable::find($id);
        $vegetable->availability_status = $status;
        $vegetable->save();

        session()->flash('message', 'Availability status has been changed successfully!');
    }
}
