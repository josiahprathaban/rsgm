<?php

namespace App\Http\Livewire\Admin;

use App\Models\Order;
use App\Models\User;
use Livewire\Component;

class DashboardComponent extends Component
{
    public $status;
    public $ids;
    public $reson;

    public function render()
    {
        $order_details = User::join('orders','orders.user_id','=','users.id')
        ->join('vegetables','vegetables.id','=','orders.vegetable_id')
        ->select('orders.*','users.name','vegetables.name as vegetable_name')
        ->where('orders.order_status','Pending')
        ->orderBy('orders.id','DESC')
        ->get();

        return view('livewire.admin.dashboard-component',compact('order_details'))->layout('admin.layouts.app');
    }

    public function changeOrderStatus($status, $id){
        $order = Order::find($id);
        $order->order_status = $status;
        $order->save();

        session()->flash('message', 'Order status has been changed successfully!');
    }

    public function edit($id)
    {
        $order = Order::where('id', $id)->first();
        $this->ids = $order->id;
        $this->status = $order->order_status;
        $this->reson = $order->canceled_reson;
        
    }

    public function update()
    {
        if ($this->ids) {
            $order = Order::find($this->ids);
            $order->order_status = "Canceled";
            $order -> canceled_reson = $this->reson;
            $order->save();

            session()->flash('message', 'Order has been canceled successfully!');
            $this->reson = '';
            $this->emit('orderCanceled');
        }
    }
}
