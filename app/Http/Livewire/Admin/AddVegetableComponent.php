<?php

namespace App\Http\Livewire\Admin;

use App\Models\Vegetable;
use Livewire\Component;

class AddVegetableComponent extends Component
{
    

    public function render()
    {
        return view('livewire.admin.add-vegetable-component');
    }

    public function clear(){
        $this->price = "";
        $this->stock = "";
        $this->name = "";
        $this->date = "";

    }
    
}
