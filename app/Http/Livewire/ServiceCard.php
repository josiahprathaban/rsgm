<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ServiceCard extends Component
{
    public $service_id;
    public $title;
    public $img_url;
    public function render()
    {
        return view('livewire.service-card');
    }
}
