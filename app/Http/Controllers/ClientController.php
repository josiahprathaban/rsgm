<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Vegetable;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        $vegetables = Vegetable::get();
        return view('index', compact('vegetables'));
    }

    public function services()
    {
        $services = Service::get();
        return view('services', compact('services'));
    }
}
