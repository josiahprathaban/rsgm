<?php

use App\Http\Controllers\ClientController;
use App\Http\Livewire\Admin\DashboardComponent;
use App\Http\Livewire\Admin\NewOrderComponent;
use App\Http\Livewire\Admin\VegetablesComponent;
use App\Http\Livewire\Admin\CanceledOrderComponent;
use App\Http\Livewire\Admin\DeliveredOrderComponent;
use App\Http\Livewire\Admin\ServicesComponent;
use App\Http\Livewire\Test\Test;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ClientController::class, 'index']);
Route::get('/services', [ClientController::class, 'services']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');




































//for customers

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    
});

//for admin

Route::middleware(['auth:sanctum', 'verified', 'authadmin'])->group(function () {
    Route::get('/admin', function () {
        return redirect('/admin/dashboard');
    });
    Route::get('/admin/dashboard', DashboardComponent::class);
    Route::get('/admin/vegetables', VegetablesComponent::class);
    Route::get('/admin/services', ServicesComponent::class);
    Route::get('/test', Test::class);
    Route::get('/admin/new-orders', NewOrderComponent::class);
    Route::get('/admin/delivered-orders', DeliveredOrderComponent::class);
    Route::get('/admin/canceled-orders', CanceledOrderComponent::class);
});
