<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
        <img src="{{asset('assets/img/logo.png')}}" style="max-width: 150px;" alt="">
        </x-slot>

        <div class="mb-4 text-sm text-gray-600">
            {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
        </div>

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="block">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="flex items-center justify-end mt-4">
            <button class="btn btn-success shadow-sm d-xl-flex justify-content-xxl-center align-items-xxl-center px-3 m-2" type="submit" style="background: linear-gradient(73deg, #077b4a, #18ae66), rgb(25,135,128);border-radius: 10px;border-width: 0px;font-weight: 400;">Email Password Reset Link</button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
