<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
        <img src="{{asset('assets/img/logo.png')}}" style="max-width: 150px;" alt="">
        </x-slot>

        <div class="mb-4 text-sm text-gray-600">
            {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
        </div>

        @if (session('status') == 'verification-link-sent')
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ __('A new verification link has been sent to the email address you provided during registration.') }}
            </div>
        @endif

        <div class="mt-4 flex items-center justify-between">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf

                <div>
                <button class="btn btn-success shadow-sm d-xl-flex justify-content-xxl-center align-items-xxl-center px-3 m-2" type="submit" style="background: linear-gradient(73deg, #077b4a, #18ae66), rgb(25,135,128);border-radius: 10px;border-width: 0px;font-weight: 400;"><button class="btn btn-success shadow-sm d-xl-flex justify-content-xxl-center align-items-xxl-center px-3 m-2" type="submit" style="background: linear-gradient(73deg, #077b4a, #18ae66), rgb(25,135,128);border-radius: 10px;border-width: 0px;font-weight: 400;">Resend Verification Email</button></button>
                </div>
            </form>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <button type="submit" class="underline text-sm text-gray-600 hover:text-gray-900">
                    {{ __('Log Out') }}
                </button>
            </form>
        </div>
    </x-jet-authentication-card>
</x-guest-layout>
