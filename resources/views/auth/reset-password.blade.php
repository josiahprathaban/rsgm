<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
        <img src="{{asset('assets/img/logo.png')}}" style="max-width: 150px;" alt="">
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $request->route('token') }}">

            <div class="block">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email', $request->email)" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="flex items-center justify-end mt-4">
            <button class="btn btn-success shadow-sm d-xl-flex justify-content-xxl-center align-items-xxl-center px-3 m-2" type="submit" style="background: linear-gradient(73deg, #077b4a, #18ae66), rgb(25,135,128);border-radius: 10px;border-width: 0px;font-weight: 400;">Reset Password</button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
