<div class="modal fade" role="dialog" tabindex="-1" id="modal-{{$service_id}}" style="border-color: #325137;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none !important;"><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button></div>
            <div class="modal-body text-center"><img src="{{asset('storage/'.$img_url)}}" height="200px">
                <h3 style="color: #325137;font-weight: bold;">{{$title}}</h3>
                <p>{{$description}}</p>
            </div>
            <div class="modal-footer"><button class="btn btn-light border rounded-pill" type="button" data-bs-dismiss="modal"><i class="la la-close me-1"></i>Cancel</button><button class="btn btn-success border rounded-pill shadow-sm" type="button" style="background: linear-gradient(73deg, #077b4a, #18ae66), rgb(25,135,128);"><i class="fa fa-phone px-2"></i>Call &amp; Order</button></div>
        </div>
    </div>
</div>