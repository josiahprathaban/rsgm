<div class="col-12">
@if (session()->has('message'))

<div class="alert alert-success" role="alert">
    {{ session('message') }}
</div>
@endif
    <div class="card">
        <div class="card-header">
            <h4>New Orders</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Order Id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Vegetable Name</th>
                            <th scope="col">Quantity(kg)</th>
                            <th scope="col">Price</th>
                            <th scope="col">Order Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($order_details as $order_detail)
                        <tr>
                            <th scope="row">{{$order_detail ->id}}</th>
                            <td>{{ $order_detail->name }}</td>
                            <td>{{$order_detail->vegetable_name}}</td>
                            <td>{{$order_detail-> quantity}}</td>
                            <td>{{$order_detail-> total}}</td>
                            <td>{{$order_detail->created_at}}</td>
                            <td>
                                @if ($order_detail->order_status == 'Pending')
                                <div class="dropdown">
                                    <button class="btn btn-info btn-rounded dropdown-toggle" type="button" id="dropdownMenuSizeButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Pending
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton2">
                                        <a class="dropdown-item" href="#" style="color: green;" wire:click.prevent="changeOrderStatus('Confirmed',{{$order_detail->id}})">Confirm</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#" style="color: red;" data-toggle="modal" data-target="#exampleModal-4" wire:click.prevent="edit({{$order_detail->id}})">Cancele</a>
                                    </div>
                                </div>
                                @elseif ($order_detail->order_status == 'Confirmed')
                                <div class="dropdown">
                                    <button class="btn btn-success btn-rounded dropdown-toggle" type="button" id="dropdownMenuSizeButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Confirmed
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton2">
                                        <a class="dropdown-item" href="#" style="color: green;" wire:click.prevent="changeOrderStatus('Delivered',{{$order_detail->id}})">Delivered</a>
                                    </div>
                                </div>
                                @endif

                            </td>


                            <td>
                                <button type="button" class="btn btn-primary btn-rounded btn-icon" data-toggle="modal" data-target="#exampleModal-3">
                                    <i class="fas fa-eye"></i>
                                </button>

                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- Modal starts -->

    <div wire:ignore.self class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel-3">Canceled Reson</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="forms-sample">
                        <div class="form-group">
                            <textarea name="reson" class="form-control" id="exampleTextarea1" rows="4" wire:model="reson" required></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button wire:click.prevent="update()" type="button" class="btn btn-success">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ends -->

</div>