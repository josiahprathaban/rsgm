<div>
  <div class="row grid-margin">
    <div class="col-12">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-column flex-md-row align-items-center justify-content-between">
            <div class="statistics-item">
              <p>
                <i class="icon-sm fa fa-user mr-2"></i>
                New users
              </p>
              <h2>54000</h2>
            </div>
            <div class="statistics-item">
              <p>
                <i class="icon-sm fas fa-hourglass-half mr-2"></i>
                Avg Time
              </p>
              <h2>123.50</h2>
            </div>
            <div class="statistics-item">
              <p>
                <i class="icon-sm fas fa-cloud-download-alt mr-2"></i>
                Downloads
              </p>
              <h2>3500</h2>
            </div>
            <div class="statistics-item">
              <p>
                <i class="icon-sm fas fa-check-circle mr-2"></i>
                Update
              </p>
              <h2>7500</h2>
            </div>
            <div class="statistics-item">
              <p>
                <i class="icon-sm fas fa-chart-line mr-2"></i>
                Sales
              </p>
              <h2>9000</h2>
            </div>
            <div class="statistics-item">
              <p>
                <i class="icon-sm fas fa-circle-notch mr-2"></i>
                Pending
              </p>
              <h2>7500</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  
  <div class="col-12">
    @if (session()->has('message'))

    <div class="alert alert-success" role="alert">
      {{ session('message') }}
    </div>
    @endif
    <div class="card">
      <div class="card-header">
        <h4>New Orders</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">Order Id</th>
                <th scope="col">Name</th>
                <th scope="col">Vegetable Name</th>
                <th scope="col">Quantity(kg)</th>
                <th scope="col">Price</th>
                <th scope="col">Order Date</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($order_details as $order_detail)
              <tr>
                <th scope="row">{{$order_detail ->id}}</th>
                <td>{{ $order_detail->name }}</td>
                <td>{{$order_detail->vegetable_name}}</td>
                <td>{{$order_detail-> quantity}}</td>
                <td>{{$order_detail-> total}}</td>
                <td>{{$order_detail->created_at}}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-info btn-rounded dropdown-toggle" type="button" id="dropdownMenuSizeButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Pending
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton2">
                      <a class="dropdown-item" href="#" style="color: green;" wire:click.prevent="changeOrderStatus('Confirmed',{{$order_detail->id}})">Confirm</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#" style="color: red;" data-toggle="modal" data-target="#exampleModal-4" wire:click.prevent="edit({{$order_detail->id}})">Cancele</a>
                    </div>
                  </div>
                </td>


                <td>
                  <button type="button" class="btn btn-primary btn-rounded btn-icon" data-toggle="modal" data-target="#orderDetailsModal">
                    <i class="fas fa-eye"></i>
                  </button>

                </td>
              </tr>
              <tr>
                <td>
                  hiii
                </td>
              </tr>
              @endforeach

            </tbody>
          </table>

        </div>
      </div>
    </div>
    <!-- Modal starts -->

    <div wire:ignore.self class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel-3">Canceled Reson</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="forms-sample">
              <div class="form-group">
                <textarea name="reson" class="form-control" id="exampleTextarea1" rows="4" wire:model="reson" required></textarea>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button wire:click.prevent="update()" type="button" class="btn btn-success">Submit</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Ends -->


    <div class="modal fade" id="orderDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel-2">Order Details of Order ID : </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <div class="template-demo">
                    <table class="table mb-0">
                    <thead>
                        <tr>
                          <th class="pl-0">Vegetables Details</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="pl-0">Icon Badge</td>
                          <td class="pr-0 text-right"><div class="badge badge-pill badge-primary"><i class="fa fa-user"></i></div></td>
                        </tr>
                        <tr>
                          <td class="pl-0">Outlined Icons</td>
                          <td class="pr-0 text-right"><div class="badge badge-pill badge-outline-info"><i class="fas fa-cloud-upload-alt"></i></div></td>
                        </tr>
                        <tr>
                          <td class="pl-0">Icon With Text</td>
                          <td class="pr-0 text-right"><div class="badge badge-pill badge-danger"><i class="far fa-envelope mr-2"></i>12</div></td>
                        </tr>
                        <tr>
                          <td class="pl-0">Icon With Text</td>
                          <td class="pr-0 text-right"><div class="badge badge-pill badge-success">12<i class="far fa-envelope ml-2"></i></div></td>
                        </tr>
                        <tr>
                          <td class="pl-0">Icon Badge</td>
                          <td class="pr-0 text-right"><div class="badge badge-pill badge-warning"><i class="fa fa-comment"></i></div></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>