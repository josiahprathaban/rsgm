<div class="col-12">
    <div class="card">
        <div class="card-header">
            <div class="row mt-3">
                <div class="col-md-8 mt-1">
                <h4>Delivered Orders</h4>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search...." wire:model="searchTerm">
                    </div>
                </div>

            </div>
           
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Order Id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Vegetable Name</th>
                            <th scope="col">Quantity(kg)</th>
                            <th scope="col">Price</th>
                            <th scope="col">Order Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($order_details as $order_detail)
                        <tr>
                            <th scope="row">{{$order_detail ->id}}</th>
                            <td>{{ $order_detail->name }}</td>
                            <td>{{$order_detail->vegetable_name}}</td>
                            <td>{{$order_detail-> quantity}}</td>
                            <td>{{$order_detail-> total}}</td>
                            <td>{{$order_detail->created_at}}</td>
                            <td>{{$order_detail->order_status}}</td>
                            <td>
                                <button type="button" class="btn btn-primary btn-rounded btn-icon" data-toggle="modal" data-target="#exampleModal-3">
                                    <i class="fas fa-eye"></i>
                                </button>

                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>