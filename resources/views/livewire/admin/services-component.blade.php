<div>
    
@if (session()->has('message'))

<div class="alert alert-success" role="alert">
    {{ session('message') }}
</div>
@endif



<div class="col-lg-12 grid-margin stretch-card">
    
    <div class="card">

        <div class="card-header">
            <h4 class="float-left pt-2">All Services </h4>


            <div class="float-right pt-2">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search...." wire:model="searchTerm">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-primary btn-icon-text" data-toggle="modal" data-target="#exampleModal-2">
                            <i class="fas fa-plus  btn-icon-prepend"></i>
                            Create
                        </button>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">
                                Img
                            </th>
                            <th class="text-center">
                                Services Name
                            </th>
                            <th class="text-center">
                            Description
                            </th>
                            <th class="text-center">
                                Price
                            </th>
                            <th class="text-center">
                                Availability
                            </th>
                            <th class="text-center">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($services as $service)
                        <tr>
                            <td class="py-1">
                                <img src="{{asset('storage/'.$service->img)}}" alt="image" />
                            </td>
                            <td class="text-center">
                                {{ $service -> name }}
                            </td>
                            <td class="text-center">
                                {{ $service -> description }}
                            </td>
                            <td class="text-center">
                                {{ $service ->price }}
                            </td>
                        
                            <td class="text-center">
                                @if ($service->availability_status == 1)
                                <div class="dropdown">
                                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuSizeButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Available
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton2">
                                        <a class="dropdown-item" href="#" style="color: red;" wire:click.prevent="changeStatus(0,{{$service->id}})" onclick="showSuccess()">Not Available</a>
                                    </div>
                                </div>

                                @else
                                <div class="dropdown">
                                    <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenuSizeButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Not Available
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton2">
                                        <a class="dropdown-item" href="#" style="color: green; " wire:click.prevent="changeStatus(1,{{$service->id}})" onclick="showSuccess()">Available</a>
                                    </div>
                                </div>

                                @endif

                            </td>
                            <td class="text-center">
                                <button type="button" class="btn btn-primary btn-rounded btn-icon" data-toggle="modal" data-target="#exampleModal-3" wire:click.prevent="edit({{$service->id}})">
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>


            </div>
        </div>
    </div>
    <!-- Modal starts -->

    <div wire:ignore.self class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel-2">Add New Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form class="forms-sample" id="addForm">
                        <div class="form-group">
                            <label>Service Name</label>

                            <input type="text" class="form-control" name="name" placeholder="Name" wire:model="name">
                            @error('name')
                            <div class="text-danger" role="alert">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                        <label>Description</label>
                            <textarea name="description" class="form-control" id="exampleTextarea1" rows="4" wire:model="description" required></textarea>
                            @error('description')
                            <div class="text-danger" role="alert">{{$message}}</div>
                            @enderror
                        </div>
        
                        <div class="form-group">
                            <label>Price</label>
                            <input type="number" class="form-control" name="price" wire:model="price" required />
                            @error('price')
                            <div class="text-danger" role="alert">{{$message}}</div>
                            @enderror
                        </div>
                       
                        <div class="form-group">
                            <label>Image</label>
                            <input type="file" class="form-control" name="image" wire:model="image" required />
                            @error('image')
                            <div class="text-danger" role="alert">{{$message}}</div>
                            @enderror
                            @if ($image)
                            Photo Preview:
                            <img src="{{ $image->temporaryUrl() }}" style="padding-top: 10px; max-width: 100%;">
                            @endif
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button wire:click.prevent="store()" type="button" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ends -->

    <!-- Modal starts -->

    <div wire:ignore.self class="modal fade" id="exampleModal-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel-2">Edit Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form class="forms-sample">
                        <div class="form-group">
                            <input type="text" name="id" wire:model="ids" hidden>
                            <label>Service Name</label>

                            <input type="text" class="form-control" name="name" placeholder="Name" wire:model="name">
                        </div>
                        <div class="form-group">
                        <label>Description</label>
                            <textarea name="description" class="form-control" id="exampleTextarea1" rows="4" wire:model="description" required></textarea>
                        </div>
                       
                        <div class="form-group">
                            <label>Price</label>
                            <input type="number" class="form-control" name="price" wire:model="price" required />
                        </div>
                       
                        <div class="form-group">
                            <label>Image</label>
                            <input type="file" class="form-control" name="image" wire:model="image" required />
                            @if ($image)
                            Photo Preview:
                            <img src="{{ $image->temporaryUrl() }}" style="max-width: 100%; padding-top: 10px;">
                            @else
                            <img src="{{asset('storage/'.$update_image)}}" style="max-width: 100%; padding-top: 10px;" />
                            @endif
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button wire:click.prevent="update()" type="button" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ends -->



</div>

</div>