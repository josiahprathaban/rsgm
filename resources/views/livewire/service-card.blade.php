<div class="card border-light shadow-sm my-3" style="border-radius: 10px;"><img class="card-img-top w-100 d-block" style="height: 200px; object-fit: scale-down;" src="{{asset('storage/'.$img_url)}}">
    <div class="card-body d-flex flex-column align-items-center">
        <h4 class="card-title" style="font-weight: bold;color: #325137;">{{$title}}<br></h4>
        <button class="btn btn-success shadow-sm d-xl-flex justify-content-xxl-center align-items-xxl-center px-3 mt-2" type="button" style="background: linear-gradient(73deg, #077b4a, #18ae66), rgb(25,135,128);border-radius: 10px;border-width: 0px;width: 100%;font-weight: 400;" data-bs-target="#modal-{{$service_id}}" data-bs-toggle="modal"><i class="fa fa-info-circle mx-2"></i>More Details</button>
    </div>
</div>