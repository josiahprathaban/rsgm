<div class="modal fade" role="dialog" tabindex="-1" id="modal-{{$product_id}}" style="border-color: #325137;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form class="modal-content" method="POST" action="/">
            <div class="modal-header" style="border-bottom: none !important;"><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button></div>
            <div class="modal-body"><img src="{{asset('storage/'.$img_url)}}" height="200px">
                <h3 style="color: #325137;font-weight: bold;">{{$title}}</h3>
                <p class="text-muted" style="margin: 0px;">Imported Date : {{date('d-m-Y', strtotime($imported_date))}}</p>
                <p>{{$stock}} Kg available</p><label class="form-label mx-2">Quantity&nbsp;</label><input class="border rounded border-success shadow-sm px-2" type="number" name="qty" required="" style="width: 80px;" value="0" min="1"><label class="form-label">&nbsp; ($ {{$price}}/Kg)<br></label>
                <p class="text-muted m-2">{{$description}}</p>
            </div>
            <div class="modal-footer"><button class="btn btn-light border rounded-pill" type="button" data-bs-dismiss="modal"><i class="la la-close me-1"></i>Cancel</button><button class="btn btn-success border rounded-pill shadow-sm" type="submit" style="background: linear-gradient(73deg, #077b4a, #18ae66), rgb(25,135,128);"><i class="fa fa-shopping-cart px-2"></i>Buy Now</button></div>
        </form>
    </div>
</div>