
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>rsgm</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Baloo+2&amp;display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&amp;display=swap">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.css">
</head>

<body style="font-family: Roboto, sans-serif;border-color: rgb(255,255,255); text-align:center">
    <nav class="navbar navbar-light navbar-expand-lg sticky-top bg-light shadow-sm navigation-clean-button">
        <div class="container"><a class="navbar-brand" href="#" style="font-weight: bold;font-size: 27px;color: #396244; letter-spacing: 2px;">RSGM</a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse text-start" id="navcol-1">
                <ul class="navbar-nav me-auto">
                    <li class="nav-item"></li>
                    <li class="nav-item"><a class="nav-link active" href="/">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="/services">Services</a></li>
                    <li class="nav-item"><a class="nav-link" href="about.html">About</a></li>
                    <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li>
                </ul><span class="navbar-text actions"> <a class="login" href="/login">Log In</a><a class="btn btn-light action-button" role="button" href="/register" style="background: linear-gradient(36deg, rgb(58,97,68), #18ae66), rgb(25,135,128);">Sign Up</a></span>
            </div>
        </div>
    </nav>
    <div class="simple-slider">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide" style="background: url(&quot;assets/img/2.jpg&quot;) center center / cover no-repeat;"></div>
                <div class="swiper-slide" style="background: url(&quot;assets/img/1.jpg&quot;) center center / cover no-repeat;"></div>
                <div class="swiper-slide" style="background: url(&quot;assets/img/3.jpg&quot;) center center / cover no-repeat;"></div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="text-white-50 swiper-button-prev"></div>
            <div class="text-white-50 swiper-button-next"></div>
        </div>
    </div>
    <div class="container flex-grow-1 my-5">
        <div class="row">
            @foreach($vegetables as $vegetable)
            <div class="col-md-3 col-sm-6">
                <livewire:product-card product_id="{{$vegetable->id}}" title="{{$vegetable->name}}" imported_date="{{$vegetable->import_date}}" price="{{$vegetable->price}}"  img_url="{{$vegetable->img}}"/>
            </div>
            <livewire:product-model product_id="{{$vegetable->id}}" title="{{$vegetable->name}}" imported_date="{{$vegetable->import_date}}" price="{{$vegetable->price}}"  img_url="{{$vegetable->img}}" stock="{{$vegetable->stock}}" description="{{$vegetable->description}}" />
            @endforeach 
        </div>
    </div>
    
    <footer class="bg-light footer-clean">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-4 col-md-3 item">
                    <h3>Services</h3>
                    <ul>
                        <li><a href="#">Web design</a></li>
                        <li><a href="#">Development</a></li>
                        <li><a href="#">Hosting</a></li>
                    </ul>
                </div>
                <div class="col-sm-4 col-md-3 item">
                    <h3>About</h3>
                    <ul>
                        <li><a href="#">Company</a></li>
                        <li><a href="#">Team</a></li>
                        <li><a href="#">Legacy</a></li>
                    </ul>
                </div>
                <div class="col-sm-4 col-md-3 item">
                    <h3>Careers</h3>
                    <ul>
                        <li><a href="#">Job openings</a></li>
                        <li><a href="#">Employee success</a></li>
                        <li><a href="#">Benefits</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-snapchat"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a>
                    <p class="copyright">Company Name © 2021</p>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.js"></script>
    <script src="assets/js/script.min.js"></script>
</body>

</html>