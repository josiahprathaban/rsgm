<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade ');

            $table->bigInteger('vegetable_id')->unsigned();
            $table->foreign('vegetable_id')->references('id')->on('vegetables')->onDelete('cascade ');

            $table->integer('quantity')->nullable();

            $table->decimal('total', 8,2)->default('0.00');
            $table->string('order_status');
            $table->string('canceled_reson')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
